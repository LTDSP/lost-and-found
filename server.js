const http = require('http');
const StubService = require('./StubService.js');

http.createServer((request, response) => { 
    const protocol = new JsonProtocol(new Transport(request, response));
    const controller = new Controller(protocol, new StubService());
    const reportError = (err) => {
        response.statusCode = 400;
        protocol.writeError(-32600, err);
        protocol.transport.flush();
    }
    
    try { 
        protocol.readMethodField()
        .then(method => dispatch(method, controller))
        .catch(reportError);
    } catch (err) { 
        reportError(err);
    }
     
}).listen(8080);

function dispatch(method, controller) { 
    switch(method) { 
        case 'list': 
            return controller.doList(); 
    } 
    throw new Error('Invalid request.'); 
}

class Controller {
    constructor(protocol, service) {
        this.protocol = protocol;
        this.service = service;
    }
    
    doList() {
        this.protocol.readListRequest().then(request => {
            const listResponse = this.service.list(request);
            this.protocol.writeListResponse(listResponse);
            this.protocol.transport.flush();    
        });
    }
}

class Transport {
    
    constructor(inStream, outStream) {
        this.inBuffers = [];
        this.outBuffers = [];
        this.inStream = inStream;
        this.outStream = outStream;
        this.inStream.on('data', chunk => {
            this.inBuffers.push(chunk);
        })
        this.promiseToInBuffer = new Promise(resolve => {
            this.inStream.on('end', () => {
                resolve(Buffer.concat(this.inBuffers).toString());
            });    
        });
    }
    
    read() {
        return this.promiseToInBuffer;
    }
    
    write(buffer) {
        if (typeof buffer === 'string')
            buffer = Buffer.from(buffer);
        this.outBuffers.push(buffer);
    }
    
    flush() {
        const response = Buffer.concat(this.outBuffers).toString();
        this.outStream.end(response);
    }
}

class JsonProtocol {
    constructor(transport) {
        this.transport = transport; 
    }
    
    readMethodField() {
        return this.transport.read().then(buffer => {
            const request = JSON.parse(buffer); // actually reads whole request...
            return request.hasOwnProperty('method') ? 
            request.method : // we know it is a JSON-RPC object, WireRpcRequest?    
            Promise.reject('Invalid request.');
        });
    }
    
    readListRequest() {
        return this.transport.read().then(buffer => {
            const request = JSON.parse(buffer);
            return request.params;
        });
    }
    
    writeListResponse(response) {
        this.transport.write(JSON.stringify({
            method: "list",
            result: response
        }));
    }
    
    writeError(code, message) {
        this.transport.write(JSON.stringify({code: code, message: message}));
    }
}