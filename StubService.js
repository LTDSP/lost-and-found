class StubService {
    list(request) {
        return {
            available: {
                basic: {
                    description: "A stub, returns sum of samples in process block and keeps a cumulative sum.",
                    identifier: "stub",
                    name: "Stub"
                },
                basicOutputInfo: [
                    {
                        description: "The sum of the samples in the input block",
                        identifier: "sum",
                        name: "Sum"
                    },
                    {
                        description: "The cumulative sum over all input blocks",
                        identifier: "cumsum",
                        name: "Cumulative Sum"
                    }
                ],
                inputDomain: 0,
                maxChannelCount: 1,
                minChannelCount: 1,
                pluginKey: "stub:sum",
                pluginVersion: 1
            }
        }
    }
}

module.exports = StubService;