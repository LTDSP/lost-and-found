# Lost & Found 

There is a small server application / script which contains a few ideas for abstractions which may or may not be useful for RPC stuff. 

```bash
npm server.js
```

There is no client currently, so sending a request from something like cURL is necessary. 

```bash
curl -X POST -d '{"method": "list"}' 127.0.0.1:8080
```
